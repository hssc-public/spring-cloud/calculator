package hu.dpc.edu.calculator;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
    @Autowired
    private AddService addService;

    @GetMapping("addService")
    @HystrixCommand(fallbackMethod = "addFallback")
    public String addWithAddService() {
        return "adding with AddService: " + addService.add(123, 456);
    }


    public String addFallback() {
        return "Service is not available";
    }
}
