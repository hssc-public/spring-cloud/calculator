package hu.dpc.edu.calculator;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("add")
public interface AddService {
    @RequestMapping(path = "/add/{num1}/{num2}")

    long add(@PathVariable("num1") long num1, @PathVariable("num2") long num2);
}
